## Levi Lugato K8s test Podchaser

. To create the cluster on AWS I'm using terraform to create the VPC and tag the subnets, the ECR to store the Docker images

. To create the Kubernetes cluster (EKS) I am using `eksctl` (obviously I could have used the terraform itself but eksctl handles EKS better and is more mature to handle the new features), to run the Pods I've choosed the `fargate` profile that is better for small applications that don't have state

. To expose the application to public access I am using an ALB ingress as ingress controller, I could use a service with a Load Balancer annotation but Fargate has a limitation about it and using an external controller we have more control about our endpoints, security and other details.

. if we had a valid domain, we could create an entry of type CNAME in Route53 pointing to the URL of the ALB ingress created and in the Ingress settings we could configure the certificate using ACM or even a let's encrypt provisioning this certificate on the side backend.

. to deploy or update a pod in a standard way we can only use kubernetes as it offers possibilities such as rollout, Canary Deployment, AB Deployment, Ramped among others, but we can also benefit from the integrations we have at the repository level to configure our cluster and enable auto devops which basically abstracts the need to create the steps so that the CDs are made with each merge or push


Here is the URL of the simple nginx application deployed on the K8S cluster created:
`http://471af262-default-podchaser-2823-376916235.us-east-1.elb.amazonaws.com/`


. The scripts folder contain the script to list '$userName $userId $userHomeDirectory'.
