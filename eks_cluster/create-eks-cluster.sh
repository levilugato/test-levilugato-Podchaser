#!/bin/bash

CLUSTER_NAME=Testlugatopodchaser

export TF_OUTPUT=$(cd ../terraform && terraform output -json)
export PRIVATE_SUBNETS="$(echo ${TF_OUTPUT} | jq -r .private_subnet_ids.value)"
export PUBLIC_SUBNETS="$(echo ${TF_OUTPUT} | jq -r .public_subnet_ids.value )"

# Create EKS cluster
eksctl create cluster --name $CLUSTER_NAME --region us-east-1  --version 1.20 --fargate  \
--vpc-private-subnets=$PRIVATE_SUBNETS \
--vpc-public-subnets=$PUBLIC_SUBNETS