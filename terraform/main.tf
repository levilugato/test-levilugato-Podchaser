terraform {
  required_version = "1.0.0"
}

provider "aws" {
    shared_credentials_file = "~/.aws/credentials"
    region =                  "${var.aws_region}"
}

# vpc
module "vpc" {
  source             = "terraform-aws-modules/vpc/aws"
  version            = "2.63.0"
  name               = var.name
  azs                = var.azs
  cidr               = "10.0.0.0/16"
  private_subnets    = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  enable_nat_gateway = true
  single_nat_gateway = true
  private_subnet_tags = {
     "kubernetes.io/role/elb" = "1"
     "kubernetes.io/cluster/${var.name}" = "shared"

  }
  public_subnet_tags = {
    "kubernetes.io/role/elb" = "1"
    "kubernetes.io/cluster/${var.name}" = "shared"
  }
}

# ecr
module "ecr" {
  source = "./modules/ecr"
  name   = "repo-podchaser"
  tags   = var.tags

}
